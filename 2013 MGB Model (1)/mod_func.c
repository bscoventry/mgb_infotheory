#include <stdio.h>
#include "hocdec.h"
#define IMPORT extern __declspec(dllimport)
IMPORT int nrnmpi_myid, nrn_nobanner_;

modl_reg(){
	//nrn_mswindll_stdio(stdin, stdout, stderr);
    if (!nrn_nobanner_) if (nrnmpi_myid < 1) {
	fprintf(stderr, "Additional mechanisms from files\n");

fprintf(stderr," GABAbDiag.mod");
fprintf(stderr," IKleak.mod");
fprintf(stderr," ILGHK.mod");
fprintf(stderr," INaLeak.mod");
fprintf(stderr," INaP.mod");
fprintf(stderr," INaP2.mod");
fprintf(stderr," INaT.mod");
fprintf(stderr," ITGHK.mod");
fprintf(stderr," Ik2.mod");
fprintf(stderr," IkDr.mod");
fprintf(stderr," VClamp.mod");
fprintf(stderr," ampa1.mod");
fprintf(stderr," ampa2.mod");
fprintf(stderr," ampa3.mod");
fprintf(stderr," cadecay.mod");
fprintf(stderr," gabaA.mod");
fprintf(stderr," gabaB.mod");
fprintf(stderr," gabaB2.mod");
fprintf(stderr," gabaB3.mod");
fprintf(stderr," gababtry.mod");
fprintf(stderr," hh2.mod");
fprintf(stderr," htc.mod");
fprintf(stderr," ia.mod");
fprintf(stderr," nmda1.mod");
fprintf(stderr," nmda2.mod");
fprintf(stderr," nmda3.mod");
fprintf(stderr," spikeout.mod");
fprintf(stderr," vecevent.mod");
fprintf(stderr, "\n");
    }
_GABAbDiag_reg();
_IKleak_reg();
_ILGHK_reg();
_INaLeak_reg();
_INaP_reg();
_INaP2_reg();
_INaT_reg();
_ITGHK_reg();
_Ik2_reg();
_IkDr_reg();
_VClamp_reg();
_ampa1_reg();
_ampa2_reg();
_ampa3_reg();
_cadecay_reg();
_gabaA_reg();
_gabaB_reg();
_gabaB2_reg();
_gabaB3_reg();
_gababtry_reg();
_hh2_reg();
_htc_reg();
_ia_reg();
_nmda1_reg();
_nmda2_reg();
_nmda3_reg();
_spikeout_reg();
_vecevent_reg();
}
