TITLE Potassium Leak Channel
UNITS {
	(mV) = (millivolt)
	(mA) = (milliamp)
	(S) = (siemens)
}

NEURON {
	SUFFIX IKLeak
	USEION k READ ek WRITE ik
	RANGE g
}

PARAMETER {
	g = .001	(S/cm2)	<0,1e9>
	ek	= -100	(mV)
}

ASSIGNED {v (mV)  ik (mA/cm2)}

BREAKPOINT {
	ik = g*(v - ek)
}
