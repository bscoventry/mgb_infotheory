function [vs,ray,spike_abs,num_spikes2,offset,spike_times3,sptimesstd] = CompleteBVSRayAnalysis_Hist(period,complete,filesav)
str1a = num2str(1);
strfin = '.dat';
newfilesav = strcat(filesav,str1a,strfin);

%Concatenate Data
[spike_timesRAW, num_spikes(1)] = BatchMGBAnalysis(newfilesav);
spike_timesR = spike_timesRAW;
sptimesstd(1).data = spike_timesRAW;

if(complete >= 2)
    for i = 2:complete
        strnum = num2str(i);
        newfilesaves = strcat(filesav,strnum,strfin);
        [spike_timesRAWD, num_spikes(i)] = BatchMGBAnalysis(newfilesaves);
        sptimesstd(i).data = spike_timesRAWD;
        spike_timesR = cat(1,spike_timesR, spike_timesRAWD);
    end
end
global duration
stim_dur = duration;
% stim_dur = 500;                    %find the duration of the stimulus                           
onset = 200;
T = period;                              %Calculate the period in ms
num_T = stim_dur/T;                 %find the number of periods per stimulus
num_T = ceil(num_T);
if (num_T < 1)
    num_T = 1;
end
p = 1;
z = 1;

for i = 1:complete
    sptimesstd(i).data = sptimesstd(i).data(sptimesstd(i).data >= onset & sptimesstd(i).data <= (onset + stim_dur));
end

% Remove entries if no spikes exist!
% rem = find(spike_timesR < 0);
% trem = isempty(rem);
% if(trem == 0)
%     for i = 1:length(rem)
%         spike_timesR(rem(i)) = [];
%     end
% end
offset = spike_timesR(spike_timesR > onset + stim_dur);
offset = offset - stim_dur;
o=1;
spike_timesRa = [];
for i= 1:length(spike_timesR)
    if(spike_timesR(i) > 0 && spike_timesR(i) <= stim_dur+onset);
        spike_timesRa(o) = spike_timesR(i);
        o = o + 1;
    else
    end
end
spike_timesRb = spike_timesRa(spike_timesRa >= 250);
spike_timesRs = spike_timesRa(spike_timesRa >= 200);
if(spike_timesRs ~=0)
spike_times3 = spike_timesRs - onset; 
else
spike_times3 = [];
end
% testb = isempty(spike_timesRa);
if(spike_timesRb ~= 0)

spike_times = spike_timesRa - onset;
spike_times2 = spike_timesRb - onset;

% test = find(spike_times < stim_dur);
% testb = isempty(test);
   
%         num_spikes2 = sum(num_spikes,2);
        num_spikes3 = length(spike_times2);
        num_spikes2 = length(spike_times);
        % loop that goes through all the spike times and determines all
        % spikes within the stimulated time
        for y=1:length(spike_times2)                                
            if(spike_times2(y) >= 0 && spike_times2(y) < stim_dur)
                for x = 0:(num_T-1)
                    if(spike_times2(y)>=0 + x*T && spike_times2(y) < (x+1)*T)
                        spike_abs(p) = spike_times2(y) - x*T;
                        p = p+1;
                    end
                end

            end

        end

        r=0;
        q=0;

        if(num_spikes ~= 0)
            k = 1;
%             e = size(spike_abs);
            e = length(spike_abs);
            for counter1=1:e
                if(spike_abs(z,counter1) > 0)
                    spike_abs2(k,1) = spike_abs(z,counter1);
                    r = r + cos((2*pi*(spike_abs2(k,1)))/T);          
                    q = q + sin((2*pi*(spike_abs2(k,1)))/T);          
                    k=k+1;
                end
            end
            vs(z)=((r^2+q^2)^.5)/num_spikes3;
            ray(z)=2*num_spikes3*vs(z)^2;
        else
            vs(z) = 0;
            ray(z) = 0;
            spike_abs = 0;
            num_spikes2 = 0;
        end
else
    vs(z) = 0;
    ray(z) = 0;
    spike_abs = 0;
    num_spikes2 = 0;
end

%%%
% Omit the first 50ms of stimulus presentation - interested only in
% sustained response, not phase-locked onset response
%
        
