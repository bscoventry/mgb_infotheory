TITLE GABAa synapse update

COMMENT
	simple inverted alpha synapse that generates a single PSP
	modified from:  
        *********************************************
        reference:      McCormick, Wang & Huguenard (1993) 
			Cerebral Cortex 3(5), 387-398
        *********************************************
ENDCOMMENT
					       
INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	POINT_PROCESS ICGABAa2
	RANGE e, g, i, tau, a1, tauF, tauR, tau1, tauR2, tauF2
	NONSPECIFIC_CURRENT i
}

UNITS {
	(nA) 	= (nanoamp)
	(mV)	= (millivolt)
	(nS) 	= (nanomho)
}

PARAMETER {
	:tauF = 4.016	(ms)
	:tauR = 345.9	(ms)
	tauF = .4157	(ms)
	tauR = 45	(ms)
	tauR2 = 13.34	(ms)
	tau = 18	(ms)
	:tau1 = 1.9604	(ms)
	tau1 = .125	(ms)
	a1 = 1
	b1 = 19.98
	e= -75		(mV)
	v		(mV)
	celsius		(degC)
}

ASSIGNED { 
	i 		(nA)  
	g 		(nS)
}

STATE {
	A (nS)
	B (nS)
}

UNITSOFF

INITIAL {

	A = 0
	B = 0
}

BREAKPOINT { 
	SOLVE state METHOD cnexp
	g = (B - A)
	:i = 1.4706*g*(v - e)
	i = 1.0425*g*(v - e)
}


DERIVATIVE state {
	A' = -A/tau1
	B' = -B/tau
}
UNITSON

NET_RECEIVE(weight (nS), t0 (ms), t1 (ms)){
LOCAL td, td2
INITIAL {t0 = t t1 = t}
td = (t-t0)
td2 = (t-t1)

if (td > 0) {
	A = A + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
	B = B + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
} else if (td2 > 0) {
	A = A + .001 * weight * (1 - (exp(-td/tauR)-exp(-td/tauF))*(1 + b1*exp(-td2/tauR2)))
	B = B + .001 * weight * (1 - (exp(-td/tauR)-exp(-td/tauF))*(1 + b1*exp(-td2/tauR2)))
} else {
	A = A + .001 * weight 
	B = B + .001 * weight 
}
t1 = t0
t0 = t
}