function MGBrMTFtMTF(periods,numtrials,size,mempot,plas,delo)
syntype = {'LargeIC','SmallIC','MidIC'};
% clear variables
close all
clear PerTrialSpk
clear PerVS2
clear PerRAY2
clear RayStat
clear VSstat
% delayed = [-5, -2, -1, 0, 1, 2, 5];
delayed = delo;
%load data files
load RAYData2
load TrialSPK2
load VSData2

lincols = {'--xb','--xr','--xc',':sr','-.dg','-.dm','-.dy'};

% periods = [3,5,7.5,10,13,15,20,25,50,100,150];

j = size;
w = 1;
k = mempot;
z = plas;

%Calculate Spike Rate
for dels = 1:length(delayed)
    clear permean
    clear perstd
    clear persterr
    clear dummy
    clear RayStat
    clear VSStat
    for i = 1:length(periods);
        for g = 1:numtrials
            dummy = length(PerTrialSpk(j,k).data(i,dels).test(g).data);
            %         set(i,z,g) = dummy/.5;
            set(g) = dummy/.5;
        end
        permean(i) = mean(set);
        perstd(i) = std(set);
        persterr(i) = perstd(i)./(sqrt(numtrials));
        % permean
        % persterr
        % Rayleigh and Vector Strength
        RayStat(i) = PerRAY2(j,k).data(i,dels);
        VSstat(i) = PerVS2(j,k).data(i,dels);
        
    end
    lincolor2 = char(lincols(dels));
    % Create Figures
    fig1 = figure(1);
    errorbar(periods,permean,persterr,lincolor2)
    xlim([1 200])
    ylim([0 150])
    hold on
    fig2 = figure(2);
    semilogx(periods,RayStat,lincolor2)
    hold on

    xlim([1 200])
    fig3 = figure(3);
    hold on
    semilogx(periods,VSstat,lincolor2)
    xlim([1 200])
    ylim([0 1])
    
end
% cd ..

figure(1)
title(strcat(char(syntype(size)),' Rate Plot'));
ylabel('Spike Rate (Sp/Sec)')
xlabel('Period (ms)')
% legend('-5ms','0ms','5ms')
figure(2)
title(strcat(char(syntype(size)),' Rayleigh Plot'));
title(char(syntype(size)));
% legend('-5ms','0ms','5ms')
tresh = 13.8*ones(1,length(periods));
semilogx(periods,tresh,'k')
ylabel('Rayleigh Statistic')
xlabel('Period (ms)')
figure(3)
% legend('-5ms','0ms','5ms')
title(strcat(char(syntype(size)),' VS Plot'));
ylabel('Vector Strength')
xlabel('Period (ms)')

saveas(fig1,strcat(char(syntype(size)),'msSpkn.fig'),'fig')
saveas(fig2,strcat(char(syntype(size)),'msRAYn.fig'),'fig')
saveas(fig3,strcat(char(syntype(size)),'msVSn.fig'),'fig')