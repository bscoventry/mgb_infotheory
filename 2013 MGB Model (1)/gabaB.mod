TITLE GABAb synapse

COMMENT
        simple alpha-synapse that generates a single PSP   
        *********************************************
        reference:   	McCormick, Wang & Huguenard (1993) 
			Cerebral Cortex 3(5), 387-398
        found in:       cat reticular nucleus of thalamus
        *********************************************
	Assembled for MyFirstNEURON by Arthur Houweling
ENDCOMMENT
					       
INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	POINT_PROCESS ICGABAb
	:USEION k READ ek 
	RANGE onset, gmaxIPSP, e, g, i, w, a, b, c, t1, t2, t3
	NONSPECIFIC_CURRENT i
}

UNITS {
	(nA) 	= (nanoamp)
	(mV)	= (millivolt)
	(nS) 	= (nanomho)
}

PARAMETER {
	onset= 25	(ms)
	gmaxIPSP= 0	(nS)
	w= .001				: weight factor for gmaxIPSP
	kf= 1				: kinetic factor
	e = -85		(mV)
	v		(mV)
	celsius		(degC)
	a = 0.84
	b = 0.16
	c = 0.31
	t1 = 283
	t2 = 1026
	t3 = 112
}

ASSIGNED { 
	i 		(nA)  
	g 		(nS)
	tadj
}

UNITSOFF
INITIAL {
        tadj = 3^((celsius-23.5)/10)
}    

BREAKPOINT { LOCAL tt
	tt = (t-onset)*tadj
	if (t>onset) {
	  :g = w* (0.84*exp(-tt/283) + 
 	   :   0.16*exp(-tt/1026)) * ((1-exp(-tt/112))^4)/0.31
	  g = w* (a*exp(-t/t1) + 
 	      b*exp(-t/t2)) * ((1-exp(-t/t3))^4)/c	

	}
	else {g = 0}
	i = g * (v-e)
}
UNITSON

NET_RECEIVE(weight (nS), t0 (ms)){
LOCAL td
INITIAL {onset = t t0 = t}
td = (t-t0)
w = .001 * weight
t0 = t
onset = t
}