function varargout = testGUI2(varargin)
% TESTGUI2 MATLAB code for testGUI2.fig
%      TESTGUI2, by itself, creates a new TESTGUI2 or raises the existing
%      singleton*.
%
%      H = TESTGUI2 returns the handle to a new TESTGUI2 or the handle to
%      the existing singleton*.
%
%      TESTGUI2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TESTGUI2.M with the given input arguments.
%
%      TESTGUI2('Property','Value',...) creates a new TESTGUI2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before testGUI2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to testGUI2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help testGUI2

% Last Modified by GUIDE v2.5 25-Mar-2013 13:29:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @testGUI2_OpeningFcn, ...
                   'gui_OutputFcn',  @testGUI2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end

% --- Executes just before testGUI2 is made visible.
function testGUI2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to testGUI2 (see VARARGIN)

% Choose default command line output for testGUI2
handles.output = hObject;

% Update handles structure
global RabangHandleData
RabangHandleData = handles;
guidata(hObject, handles);
x = 2
% UIWAIT makes testGUI2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);
end

% --- Outputs from this function are returned to the command line.
function varargout = testGUI2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to numtrials (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global numtrials
numtrials = str2double(get(hObject,'String'));
end

% Hints: get(hObject,'String') returns contents of numtrials as text
%        str2double(get(hObject,'String')) returns contents of numtrials as a double


% --- Executes during object creation, after setting all properties.
function numtrials_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numtrials (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function numIN_Callback(hObject, eventdata, handles)
% hObject    handle to numIN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global numINs
numINs = str2double(get(hObject,'String'));
end
% Hints: get(hObject,'String') returns contents of numIN as text
%        str2double(get(hObject,'String')) returns contents of numIN as a double


% --- Executes during object creation, after setting all properties.
function numIN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numIN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function delays_Callback(hObject, eventdata, handles)
% hObject    handle to delays (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global delays
delays = str2double(get(hObject,'String'));
end

% Hints: get(hObject,'String') returns contents of delays as text
%        str2double(get(hObject,'String')) returns contents of delays as a double


% --- Executes during object creation, after setting all properties.
function delays_CreateFcn(hObject, eventdata, handles)
% hObject    handle to delays (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function gAcond_Callback(hObject, eventdata, handles)
% hObject    handle to gAcond (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global gabaAcond
gabaAcond = str2double(get(hObject,'String'));
end

% Hints: get(hObject,'String') returns contents of gAcond as text
%        str2double(get(hObject,'String')) returns contents of gAcond as a double


% --- Executes during object creation, after setting all properties.
function gAcond_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gAcond (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function gBcond_Callback(hObject, eventdata, handles)
% hObject    handle to gBcond (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global gabaBcond
gabaBcond = str2double(get(hObject,'String'));
end

% Hints: get(hObject,'String') returns contents of gBcond as text
%        str2double(get(hObject,'String')) returns contents of gBcond as a double


% --- Executes during object creation, after setting all properties.
function gBcond_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gBcond (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


% --- Executes during object creation, after setting all properties.
function dirsav_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dirsav (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global valmV
contents = cellstr(get(hObject,'String'));
valmV = contents{get(hObject,'Value')};
% valmV = str2double(contents{get(hObject,'Value')});
end

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function dirsav_Callback(hObject, eventdata, handles)
% hObject    handle to dirsav (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dirsav as text
%        str2double(get(hObject,'String')) returns contents of dirsav as a double
global dsav
dsav = get(hObject,'String');
mkdir(dsav)
end

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global synsize
% synsize = get(get(PlasType,'SelectedObject'),'Max');
global RabangHandleData
popupmenu1_Callback(RabangHandleData.popupmenu1)
edit1_Callback(RabangHandleData.numtrials)
numIN_Callback(RabangHandleData.numIN)
delays_Callback(RabangHandleData.delays)
gAcond_Callback(RabangHandleData.gAcond)
gBcond_Callback(RabangHandleData.gBcond)
dirsav_Callback(RabangHandleData.dirsav)
RabangMGB2k12vGUIonly
end
