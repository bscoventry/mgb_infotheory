% Analysis Tool for NEURON traces

function [spiketime, numspikes] = BatchMGBAnalysis(name)

if (nargin < 1)

close all 
clear all
name = input('Enter filename: ','s');
data = dlmread(name,'\t');

end

data = dlmread(name,'\t');

%
% Graph trace, #Spikes, Spike Times, Spike Amplitude, ISI
%
dt = .02;
shift = 1/dt;
% InputFig(name)
spike = SpDetect(data);
if(spike ~= 0)
    spiketime = spike/shift;
    numspikes = length(spike);
else
    spiketime = -1;
    numspikes = 0;
end


function Spike = SpDetect(fdata)

thresh = -15;

len = length(fdata);
dt = .02;
shift = 1/dt;
t = linspace(0,((len-1)/shift),len);
t = t';

derivDATA = diff(fdata)./diff(t);
v = 1;
derivDATA = cat(v,derivDATA,1);

d2DATA = diff(derivDATA)./diff(t);
d2DATA = cat(v,d2DATA,1);

% Spike2 = find(fdata >= thresh & fdata <= (thresh+2) & derivDATA > 0);
Spike2 = find(fdata >= thresh & derivDATA > 0);
if(Spike2 ~= 0)

spiktim(1) = Spike2(1);
o = 2;
for i = 2:length(Spike2)
    difs = Spike2(i) - Spike2(i-1);
    if(difs >= 10)
        spiktim(o) = Spike2(i);
        o = o + 1;
    else
    end
end
Spike = spiktim';
else
Spike = [];
end
    

function InputFig(name)

data = dlmread(name,'\t');

len = length(data);
dt = .02;
shift = 1/dt;
t = linspace(0,((len-1)/shift),len);
t = t';
figure
plot(t,data)
ylim([-70 -45])





