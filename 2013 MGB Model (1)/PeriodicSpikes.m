function [sp2]=PeriodicSpikes(period)

duration = 500;
spvec = 0:period:duration;
spvec(1) = .0001;

% spprob = 1 - (exp(-(period-10)/4)./(1+exp(-(period-10)/4)));
% spprob = exp(-(period-8)/3)./(1+exp(-(period-8)/3));

% Band Pass
spprob = 1 - exp(-(period-8)/2)./(1+exp(-(period-8)/2));

if spprob > 1
    spprob = 1;
end

spnum = binomialrnd(1,spprob,1,length(spvec));

sp = spvec.*spnum;
sort(sp);
sp(sp <=0) = [];
sp2 = sp';
