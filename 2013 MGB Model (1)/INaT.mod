TITLE Fast, Transient Na+ Ionic Channel

INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	SUFFIX INat
	USEION na READ ena WRITE ina
	RANGE gnabar
	RANGE m_inf, h_inf
	RANGE tau_m, tau_h
	RANGE m_exp, h_exp
}


UNITS {
	(mA) = (milliamp)
	(mV) = (millivolt)
}

PARAMETER {
	gnabar	= .003 	(mho/cm2)
	ena	= 45	(mV)
	celsius = 36    (degC)
	dt              (ms)
	v               (mV)
}

STATE {
	m h
}

ASSIGNED {
	ina	(mA/cm2)
	m_inf
	h_inf
	tau_m
	tau_h
	m_exp
	h_exp
	tadj
}


BREAKPOINT {
	SOLVE states
	ina = gnabar * m*m*m*h * (v - ena)
}


PROCEDURE states() {	: exact when v held constant
	evaluate_fct(v)
	m = m + m_exp * (m_inf - m)
	h = h + h_exp * (h_inf - h)
	VERBATIM
	return 0;
	ENDVERBATIM
}

UNITSOFF
INITIAL {
	m = 0
	h = 0
:
:  Q10 was assumed to be 3 for both currents
:
: original measurements at roomtemperature?

	tadj = 3.0 ^ ((celsius-36)/ 10 )
}

PROCEDURE evaluate_fct(v(mV)) { LOCAL a,b


	a = 0.32 * (-(v + 55)) / ( exp(-(v + 55)/4) - 1)
	b = 0.28 * (v + 28) / ( exp((v + 28)/5) - 1)
	tau_m = 1 / (a + b) / tadj
	m_inf = a / (a + b)

	a = 0.128 * exp(-(v + 51)/18)
	b = 4 / ( 1 + exp(-(v + 28)/5) )
	tau_h = 1 / (a + b) / tadj
	h_inf = a / (a + b)

	m_exp = 1 - exp(-dt/tau_m)
	h_exp = 1 - exp(-dt/tau_h)
}

UNITSON
