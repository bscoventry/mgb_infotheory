TITLE GABAb synapse

COMMENT
        simple alpha-synapse that generates a single PSP   
        *********************************************
        reference:   	McCormick, Wang & Huguenard (1993) 
			Cerebral Cortex 3(5), 387-398
        found in:       cat reticular nucleus of thalamus
        *********************************************
	Assembled for MyFirstNEURON by Arthur Houweling
ENDCOMMENT
					       
INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	POINT_PROCESS ICGABAb2
	RANGE e, g, i, tau, a1, tauF, tauR, tau1, tau2, a1
	NONSPECIFIC_CURRENT i
}

UNITS {
	(nA) 	= (nanoamp)
	(mV)	= (millivolt)
	(nS) 	= (nanomho)
}

PARAMETER {
	a1 = .6237
	tauF = 4.016	(ms)
	tauR = 345.9	(ms)
	tau = 1026	(ms)
	tau1 = 283	(ms)
	tau2 = 112	(ms)
	e = -85		(mV)
	v		(mV)
	celsius		(degC)
}

ASSIGNED { 
	i 		(nA)  
	g 		(nS)
}

STATE {
	A	(nS)
	B	(nS)
	C	(nS)
	D	(nS)
	E	(nS)
	F	(nS)
	G	(nS)
	H	(nS)
	I	(nS)
	J	(nS)
}

UNITSOFF
INITIAL {

	A = 0
	B = 0
	C = 0
	D = 0
	E = 0
	F = 0
	G = 0
	H = 0
	I = 0
	J = 0
}

BREAKPOINT { 
	SOLVE state METHOD cnexp
	g = (84/31)*A + (16/31)*B + (84/31)*C + (504/31)*D - (336/31)*E + (16/31)*F + (96/31)*G - (64/31)*H - (336/31)*I - (64/31)*J
	i = g*(v - e)
}

DERIVATIVE state {
	A' = -A/283
	B' = -B/1026
	C' = -C/25.4791
	D' = -D/46.7493
	E' = -E/80.243
	F' = -F/27.2562
	G' = -G/53.1017
	H' = -H/100.9772
	I' = -I/32.9823
	J' = -J/36.0226
}

UNITSON


NET_RECEIVE(weight (nS), t0 (ms)){
LOCAL td
INITIAL {t0 = t}
td = (t-t0)

A = A + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
B = B + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
C = C + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
D = D + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
E = E + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
F = F + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
G = G + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
H = H + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
I = I + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
J = J + .001 * weight * (1 - (a1*exp(-td/tauR) - a1*exp(-td/tauF)))

t0 = t
}