TITLE IC IN/EX NMDA Synapse

COMMENT
	simple alpha-synapse that generates a single PSP 
        *********************************************
        reference:      McCormick, Wang & Huguenard (1993)
                        Cerebral Cortex 3(5), 387-398
        *********************************************
	Assembled for MyFirstNEURON by Arthur Houweling
ENDCOMMENT
					       
INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	POINT_PROCESS IC_NMDA_PPF
	USEION mg READ mgo VALENCE 2
	RANGE e, g, i, w, tau, a1, tauF, tauR, tau1
	NONSPECIFIC_CURRENT i
}

UNITS {
	(nA) 	= (nanoamp)
	(mV)	= (millivolt)
	(nS) 	= (nanomho)
	(mM)    = (milli/liter)
        F	= 96480 (coul)
        R       = 8.314 (volt-coul/degC)

}

PARAMETER {
	:tau = 71
	tau = 40	(ms)
	tau1 = 13.2	(ms)
	tauF = .3	(ms)
	tauR = 13.3	(ms)		
	a1 = 3.65	
	w= .001				: weight factor for gmaxEPSP
	e= 0		(mV)
	v		(mV)
	mgo		(mM)
	celsius 	(degC)
}

ASSIGNED { 
	i (nA)  
	g (nS)
	tadj
}
STATE {
	A (nS)
	B (nS)
}

UNITSOFF
INITIAL {

	A = 0
	B = 0
}



BREAKPOINT {	LOCAL k
	SOLVE state METHOD cnexp
	:k = 1.07 * exp(2*0.73*v*(.001)*F/(R*(celsius+273.16)))
	k = 1/(1 + 0.28*exp(-0.062*v))
	g = (B - A)
	:i = 2.5*g*(1-(1/(1+k/mgo)))*(v - e)
	i = 2.5*g*k*(v - e)

}

DERIVATIVE state {
	A' = -A/tau1
	B' = -B/tau
}

UNITSON

NET_RECEIVE(weight (nS), t0 (ms)){
LOCAL td
INITIAL {t0 = t}
td = (t-t0)

A = A + .001 * weight  * (1 + (a1*exp(-td/tauR) - a1*exp(-td/tauF)))
B = B + .001 * weight  * (1 + (a1*exp(-td/tauR) - a1*exp(-td/tauF)))

t0 = t
}
