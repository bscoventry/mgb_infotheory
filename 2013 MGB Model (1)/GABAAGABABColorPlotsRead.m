function GABAAGABABColorPlotsRead(period)
% direct = strcat('Period',num2str(period));
% cd(direct)
numtrials = 10;
numINs = 1;
done = numtrials;

% delayed = [-5, -3, -2, -1, 0, 1, 2, 3, 5];
delayed = 0;
k = 3;
gabaAconds = linspace(0,10,6);
gabaBconds = [0,1,2,3,4];

strB = num2str(period);


%% Basic Parameter Settings

LargeIC = {[16,.5],[2,1],[2]};
SmallIC = {[2,2],[4,1],[3]};
MidIC = {[2,2],[4,1],[4]}; %Mixed or PPF

syntype = {'LargeIC','SmallIC','MidIC'};
plastype = {'_None_','_PPD_','_Mixed_','_PPF_'};

vinit = [-55,-58,-60,-62,-65,-70,-75];
BiasCur = [.187,.15,.124,.094,.044,-.067,-.22];

H{1} = LargeIC;
H{2} = SmallIC;
H{3} = MidIC;

% YY = 18;
% dlmwrite('gabaatau.dat',YY);
% 
% ZZ = 2.8;
% dlmwrite('ampatau.dat',ZZ);

% 1 = Large, 2 = Small
% Large = 1;
% Small = 2;
% Mid = 3;

%Change between Large or Small or Mid
% synsize = Small;
% synsize = Large;

%% Synaptic Inputs!!!
% numberinputs = H{synsize}{2}(1);
% variance = H{synsize}{2}(2);



% dlmwrite('numinputs.dat',numberinputs);
% dlmwrite('numinputsI.dat',numINs);

%% Major Loop
% for z = 1:length(periods)
for synsize = 1:3
    for ga = 1:length(gabaAconds)
        for gab = 1:length(gabaBconds)
            
            
%             INdelay = delayed(del);
            gabaBcond = gabaBconds(gab);
            gabaAcond = gabaAconds(ga);
            s = H{synsize}{3};
            headpptype = char(plastype(s));
            % Define a lot of string ids
            head0 = 'GABAb';
            head0a = num2str(gabaBcond);
            head0b = 'GABAa';
            head0c = num2str(gabaAcond);
            head1 = strB;
            head2 = '_Period_';
            headtype = char(syntype(synsize));
            head4 = 'Trial_';
            
            % Create the filename to save plot to
            
            filesav = strcat(head0,head0a,head0b,head0c,head2,head1,headpptype,headtype,head4);
            [VScomp(ga,gab),RAYcomp(ga,gab),sphist.test,NCSpk(ga,gab),offsets.times,RAWspHist(ga,gab).test,spiketrials.test] = CompleteBVSRayAnalysis_Hist(period,done,filesav);
            
        end
    end
    
    RAYcomp2 = RAYcomp;
    RAYcomp2(find(RAYcomp >= 13.8)) = 1;
    RAYcomp2(find(RAYcomp <13.8)) = 0;
    
    PerHists2(synsize).data = sphist;
    PerVS2(synsize).data = VScomp;
    PerRAY2(synsize).data = RAYcomp;
    PerSPK2(synsize).data = NCSpk;
    PerHistRAW(synsize).data = RAWspHist;
    PerRAY3(synsize).data = RAYcomp2;
    PerTrialSpk(synsize).data = spiketrials;
end

save('VSData2.mat','PerVS2')
save('RAYData2.mat','PerRAY2')
save('SpkCount2.mat','PerSPK2')
save('HistData2.mat','PerHists2')
save('RAWHistData2.mat','PerHistRAW')
save('TrialSPK2.mat','PerTrialSpk')
save('RAYadj.mat','PerRAY3')
% cd .. 
