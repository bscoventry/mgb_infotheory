function MGB_NSF2013

numtrials = input('Number of Trials:');
if isempty(numtrials)
    numtrials = 10;
end

synsize = input('Plasticity Type (1)Large, (2)Small/Mixed (3)Small/PPF:');
if isempty(synsize)
    synsize = 2;
end

probs = input('Input Probability (Percent)');
if isempty(probs)
    probs = 100;
end
probs = probs/100;

% 
% numINs = input('Number of INHIBITORY inputs:');
% if isempty(numINs)
%     numINs = 1;
% end
numINs = 1;
% delays = input('Inhibitory Input Delay (ms):');
% if isempty(delays)
%     delays = 0;
% end
delayed = 0;
k = input('Membrane Potential: 1)-55, 2)-58, 3)-60, 4)-62, 5)-65, 6)-70, 7)-75');
if isempty(k)
    k = 3;
end

% gabaAcond = input('GABAa Conductance (default 4nS):');
% if isempty(gabaAcond)
%     gabaAcond = 4;
% end

% gabaBcond = input('GABAb Conductance (default 2nS):');
% if isempty(gabaAcond)
%     gabaBcond = 2;
% end

% periods = input('Input Period (ms):');
% if isempty(periods)
%     periods = [25,100];
% end

% delayed = [-5, -2, -1, 0, 1, 2, 5];
% delayed = [-5,0,5];
% periods = [3,5,7.5,10,13,15,20,25,30,40,50,100,150];
% periods = [5,15,20];
% periods = [5,10,15,25,50];
% periods = 100;
% periods = [5,7.5,10,12.5,15,20,25,30,50,100];
periods = [20];

%% Basic Parameter Settings

LargeIC = {[16,.5],[2,1],[2]};
SmallIC = {[2,2],[4,1],[3]};
MidIC = {[2,2],[4,1],[4]}; %Mixed or PPF

syntype = {'LargeIC','SmallIC','MidIC'};
plastype = {'_None_','_PPD_','_Mixed_','_PPF_'};

vinit = [-55,-58,-60,-62,-65,-70,-75];
BiasCur = [.187,.15,.124,.094,.044,-.067,-.22];

H{1} = LargeIC;
H{2} = SmallIC;
H{3} = MidIC;

YY = 18;
dlmwrite('gabaatau.dat',YY);

ZZ = 2.8;
dlmwrite('ampatau.dat',ZZ);

% 1 = Large, 2 = Small
% Large = 1;
% Small = 2;
% Mid = 3;

%Change between Large or Small or Mid
% synsize = Small;
% synsize = Large;

%% Synaptic Inputs!!!
numberinputs = H{synsize}{2}(1);
variance = H{synsize}{2}(2);


% INdelay = delays;
dlmwrite('numinputs.dat',numberinputs);
dlmwrite('numinputsI.dat',numINs);

numins = 2;  % How many synaptic types?
%% Major Loop
for z = 1:length(periods)
    for i = 1:numtrials
        for dels = 1:length(delayed)
        %         for w = 1:9
        % Choose Period/Modulation Frequency
        period = periods(z);
        
        % Create IC Input Waveform
        %         spcycle = scyclefits(period,type);
        %         cyclesd = .2;
        %         A = psth_to_vsApure(period,spcycle,cyclesd);
        gausswid = [2.4;1.15;.7;.5;.35;.25;.18;.12;.05];
        %         gaussVS =  [.9;.8;.7;.6;.5;.4;.3;.2;.1];
        w = 1;
        gausswid1 = gausswid(w);
        
        % Set the number of inputs here
        numinputs = numberinputs;
        
        for o = 1:numinputs
%             tempinputs = PeriodicSpikes(period);
            tempinputs = PoissonInputs(period,probs);
            %             tempinputs = psth_to_vsApure(period,spcycle,cyclesd);
            %             tempinputs = psth_to_vsAII(period,spcycle,cyclesd,gausswid1);
            %             tempshift = 25*randn(1);
            
            % Set the Jitter Here
            %             A(o).list = tempinputs;
            
            %             tempshift = variance*randn(1);
            tempshift = variance*randn(length(tempinputs),1);
            finished = tempinputs + tempshift;
%             finished(finished < 0) = 0;
            A(o).list = finished;
            %             str0 = 'ICTestData\';
            strB = num2str(period);
            %             str1 = '_Period _';
            %             stroo = '_RunData_';
            %             str6 = num2str(o);
            %             str2 = '.dat';
            %             stc = num2str(i);
            %             fnames = strcat(str0,strB,str1,str6,stroo,stc,str2);
            %             dlmwrite(fnames,A(o).list);
            filenames = strcat('dumbfile',num2str(o),'.dat');
            dlmwrite(filenames,A(o).list)
        end
        %         A1 = psth_to_vsAII(period,spcycle,cyclesd,gausswid1);
        %         A2 = psth_to_vsAII(period,spcycle,cyclesd,gausswid1);
        %         A = psth_to_vsApure(period,spcycle,cyclesd);
        %         gausswid1 = 1;
        % Save the Original IC Inputs
        for oi = 1:numINs
            INdelay = delayed(dels);
            tempin = PeriodicSpikes(period);
            %             tempinputs = psth_to_vsApure(period,spcycle,cyclesd);
            %             tempinputs = psth_to_vsAII(period,spcycle,cyclesd,gausswid1);
            %             tempshift = 25*randn(1);
            
            % Set the Jitter Here
            %             A(o).list = tempinputs;
            
            %             tempshiftIN = variance*randn(1);
            tempshiftIN = variance*randn(length(tempin),1);
            INfinished = tempin + tempshiftIN + INdelay;
%             INfinished(INfinished < 0) = [];
            I(oi).list = INfinished;
            %             str0 = 'ICTestData\';
            %             strB = num2str(period);
            %             str1 = '_Period _';
            %             stroo = '_RunData_';
            %             str6 = num2str(o);
            %             str2 = '.dat';
            %             stc = num2str(i);
            %             fnames = strcat(str0,strB,str1,str6,stroo,stc,str2);
            %             dlmwrite(fnames,A(o).list);
            Ifile = strcat('idumbfile',num2str(oi),'.dat');
            dlmwrite(Ifile,I(oi).list)
        end
        %% Program Initializations
        
        B = H{synsize}{1}(1);
        C = H{synsize}{1}(2);
        F = B.*C;
        D = 0;
        G = 0;
%         D = gabaAcond;
%         G = gabaBcond;
        
        E = [B;F;D;G];
        %             E = [0;0;D;G];
        
        dlmwrite('paramfile.dat',E);
        BiasAmp = BiasCur(k);
        Vint = vinit(k);
        dlmwrite('biascur.dat',BiasAmp);
        dlmwrite('vinit.dat',Vint);
        
        s = H{synsize}{3};
        
        %                 clear runtype
%         headpptype = char(plastype(s));
        % Define a lot of string ids
        head0 = 'Delay_';
        head0a = num2str(INdelay);
        head1 = strB;
        head2 = 'Period_';
        headtype = char(syntype(synsize));
        head3 = '_mempot';
        headmem = num2str(vinit(k));
        head4 = 'Trial_';
        headtrial = num2str(i);
        headfin = '.dat';
        gauss1 = strcat('_',num2str(gausswid1),'_');
        
        % Create the filename to save plot to
        filesav = strcat(head0,head0a,head2,head1,headtype,head3,headmem,head4,headtrial,headfin);
%         filesav = strcat(head2,head1,headpptype,headtype,gauss1,head3,headmem,head4,headtrial,headfin);
        fid = fopen('filesav.dat', 'wt');
        fprintf(fid, '%s', filesav);
        fclose(fid)
        
        if s == 1
            [status,result] = system('C:\nrn71\bin\nrniv.exe -nobanner SingleCompNeuron4.hoc -c quit()');
        elseif s == 2
            [status,result] = system('C:\nrn71\bin\nrniv.exe -nobanner SingleCompNeuronPPD4.hoc -c quit()');
        elseif s == 3
            [status,result] = system('C:\nrn71\bin\nrniv.exe -nobanner SingleCompNeuronMix4.hoc -c quit()');
        else
            [status,result] = system('C:\nrn71\bin\nrniv.exe -nobanner SingleCompNeuronPPF4.hoc -c quit()');
        end
        end
    end
end
global duration
synsiz = synsize;
plas = s;
mempot = k;
Inhibition_AnalysisIII(periods,numtrials,synsiz,mempot,plas,delayed)
MembraneFluctAnalysis(periods,numtrials,synsiz,mempot,plas,delayed)
load TrialSPK2
for g = 1:numtrials
    dummy = length(PerTrialSpk(synsiz,mempot).data(length(period),length(dels)).test(g).data);
    %         set(i,z,g) = dummy/.5;
    set(g) = dummy/(duration/1000);
end
permean = mean(set);
perstd = std(set);
persterr = perstd./(sqrt(numtrials));
% BatchPrintPlotsII(periods,plastype,numtrials,synsiz,mempot,plas)
% load RAYData2
% load TrialSPK2
% load VSData2
% numeranal2
save('Rate2.mat','permean')
save('RateStd2.mat','perstd')
save('RateStE2.mat','persterr')
fname = input('Save Data to New Folder:', 's');
mkdir(fname)
string2 = strcat('../MGB-IN_Model Code/',fname);
movefile('*2.mat',string2);
movefile('*mempot*',string2);


% movefile('*.fig',string2);
