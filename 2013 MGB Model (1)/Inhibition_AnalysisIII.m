function Inhibition_AnalysisIII(periods,done,synsiz,mempot,plas,delo)

syntype = {'LargeIC','SmallIC','MidIC'};

vinit = [-55,-58,-60,-62,-65,-70,-75];
% j = synsiz;
k = mempot;
z = plas;
% delayed = [-5, -2, -1, 0, 1, 2, 5];
delayed = delo;
% gausswid = [2.4;1.15;.7;.5;.35;.25;.18;.12;.05];
% gaussVS =  [.9;.8;.7;.6;.5;.4;.3;.2;.1];
% w = 1;

for dels = 1:length(delayed)
for i = 1:length(periods)
    INdelay = delayed(dels);
    head0 = 'Delay_';
    head0a = num2str(INdelay);
% type = char(deptype(z));
w = 1;    
% gausswid1 = gausswid(w);
period = periods(i);
% strfold = 'Period_';
% strfoldn = num2str(period);
% strfolding = strcat(strfold,strfoldn,'\');
headtype = char(syntype(synsiz));
strB = num2str(period);
head1 = strB;
head2 = 'Period_';
head3 = '_mempot';
headmem = num2str(vinit(k));
head4 = 'Trial_';
% gauss1 = strcat('_',num2str(gausswid1),'_');
filesav = strcat(head0,head0a,head2,head1,headtype,head3,headmem,head4);
% filesav = strcat(head2,head1,type,headtype,gauss1,head3,headmem,head4);
[VScomp(i,dels),RAYcomp(i,dels),sphist(i,dels).test,NCSpk(i,dels),offsets(i,dels).times,RAWspHist(i,dels).test,spiketrials(i,dels).test] = CompleteBVSRayAnalysis_Hist(period,done,filesav);
end
end
PerHists2(synsiz,k).data = sphist;
PerVS2(synsiz,k).data = VScomp;
PerRAY2(synsiz,k).data = RAYcomp;
PerSPK2(synsiz,k).data = NCSpk;
PerHistRAW(synsiz,k).data = RAWspHist;
PerTrialSpk(synsiz,k).data = spiketrials;


save('VSData2.mat','PerVS2')
save('RAYData2.mat','PerRAY2')
save('SpkCount2.mat','PerSPK2')
save('HistData2.mat','PerHists2')
save('RAWHistData2.mat','PerHistRAW')
save('TrialSPK2.mat','PerTrialSpk')