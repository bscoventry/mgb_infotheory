function [sp2]=PoissonInputs(period,probs)

global duration
duration = 250;
% spvec = 0:period:duration;
numcycles = duration/period;


a = 0;
for i = 1:numcycles
    spvec(i) = a + poissrnd(period,1);
    a = spvec(i);
end
spvec(1) = .0001;

% spprob = 1 - (exp(-(period-10)/4)./(1+exp(-(period-10)/4)));
% spprob = exp(-(period-8)/3)./(1+exp(-(period-8)/3));

spvec(spvec > duration) = [];

% Band Pass
% spprob = 1 - exp(-(period-8)/2)./(1+exp(-(period-8)/2));

% if spprob > 1
%     spprob = 1;
% end

spnum = binomialrnd(1,probs,1,length(spvec));

sp = spvec.*spnum;
sort(sp);
sp(sp <=0) = [];
sp2 = sp';
