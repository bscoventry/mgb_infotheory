TITLE Sodium Leak Channel
UNITS {
	(mV) = (millivolt)
	(mA) = (milliamp)
	(S) = (siemens)
}

NEURON {
	SUFFIX INaLeak
	USEION na READ ena WRITE ina
	RANGE g
}

PARAMETER {
	g = .001	(S/cm2)	<0,1e9>
	ena	= 50	(mV)
}

ASSIGNED {v (mV)  ina (mA/cm2)}

BREAKPOINT {
	ina = g*(v - ena)
}
