TITLE Persistent Sodium current, INaP

COMMENT
	Implemenented by C. Rabang, 2009 - modeled from McCormick, DA & Huguenard JA
	J Neurophysiol 68:1384-1400, 1992
ENDCOMMENT

INDEPENDENT { t FROM 0 TO 1 WITH 1 (ms) }

UNITS {
	(mV) = (millivolt)
	(mA) = (milliamp)
}
NEURON {
	SUFFIX iNap
	USEION na READ ena WRITE ina
	RANGE gnapbar
	RANGE m_inf
	RANGE tau_m
}

PARAMETER {
	gnapbar = .00003   (mho/cm2)
	ena	= 50	   (mV)
	celsius = 36	   (degC)
	v		   (mV)
}

STATE {
	m
}

ASSIGNED {
	ina (mA/cm2)
	minf
	taum  (ms)
}

BREAKPOINT {
	SOLVE states METHOD cnexp
	ina = gnapbar*m*(v - ena)
}

DERIVATIVE states {
	compute_fcn(v)
	m' = (minf - m) / taum
}

INITIAL {
	m = 0	
	:tadj = 3.0 ^ ((celsius-36)/10)
}

PROCEDURE compute_fcn(v(mV)) { LOCAL a,b
	
	a = 0.091 * (v + 38) / (1 - exp(-(v + 38)/5))
	b = -0.062 * (v + 38) / (1 - exp((v +38)/5))
	:taum = 1/(a+b)/tadj
	taum = 1/(a+b)

	minf = 1/(1 + exp((-49 - v)/5))
}