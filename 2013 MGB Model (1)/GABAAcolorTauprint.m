% function BatchCompleteCompositeTestRead
function GABAAcolorTauprint(period)

% direct = strcat('Period',num2str(period));
% cd(direct)
numtrials = 10;
done = numtrials;
gabaataus = [9,13.5,18,22.5,27];
delayed = [-5, -3, -2, -1, 0, 1, 2, 3, 5];
gabaAconds = linspace(0,20,11);
plastype = {'Large PPD','Small Mixed','Small PPF'};
strB = num2str(period);
load VSData2.mat
load RAYData2.mat
load SpkCount2.mat
load RAYadj.mat

%% Basic Parameter Settings

figure(1)
figure(2)
figure(3)
fig = figure(4);
a = 1;

for s = 1:3
    scratch = [];
    scratch = PerVS2(s).data;
%     dirs = char(plastype(s));
    fig1 = figure(1)
    subplot(4,4,a)
    imagesc(delayed,gabaataus,scratch)
    colorbar
    caxis([0 1])
    VStit1 ='Vector Strength Map: Period ';
    VStit3 = strcat('-',char(plastype(s)));
    title(strcat(VStit1,num2str(period),VStit3))
    
    
    fig2 = figure(2)
    scratch = [];
    scratch = PerRAY2(s).data;
%     scratch = scratch';
    
    subplot(4,4,a)
    imagesc(delayed,gabaataus,scratch)
    colorbar
    RAYtit1 ='Rayleigh Stat Map: Period ';
    RAYtit3 = strcat('-',char(plastype(s)));
    title(strcat(RAYtit1,num2str(period),RAYtit3))
    
    
    fig3 = figure(3)
    scratch = [];
    scratch = PerSPK2(s).data;
%     scratch = scratch';
    
    
    subplot(4,4,a)
    imagesc(delayed,gabaataus,scratch)
    colorbar
    SPKtit1 ='Spike Count Map: Period ';
    SPKtit3 = strcat('-',char(plastype(s)));
    title(strcat(SPKtit1,num2str(period),SPKtit3))
    
    fig4 = figure(4)
    scratch = [];
    scratch = PerRAY3(s).data;
%     scratch = scratch';
    subplot(4,4,a)
    imagesc(delayed,gabaataus,scratch)
    g = colorbar
    % set(fig,'Colormap',mycmap)
    set(g,'YTick',[0,1])
    
    
    RStit1 ='Rayleigh Stat Map Adj: Period ';
    RStit3 = strcat('-',char(plastype(s)));
    title(strcat(RStit1,num2str(period),RStit3))
    
    set(g,'YTickLabel',{'Nonsynchronized','Synchronized'})
    a = a + 4;

end

saveas(fig1,strcat(num2str(period),'msVS.fig'),'fig')
saveas(fig2,strcat(num2str(period),'msRAY.fig'),'fig')
saveas(fig3,strcat(num2str(period),'msSpk.fig'),'fig')
saveas(fig4,strcat(num2str(period),'msRAYadj.fig'),'fig')

% cd ..
