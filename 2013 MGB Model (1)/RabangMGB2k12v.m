function RabangMGB2k12v
global duration
duration = 500;
numtrials = input('Number of Trials:');
if isempty(numtrials)
    numtrials = 10;
end

synsize = input('Plasticity Type (1)Large, (2)Small/Mixed (3)Small/PPF:');
if isempty(synsize)
    synsize = 2;
end

numINs = input('Number of INHIBITORY inputs:');
if isempty(numINs)
    numINs = 1;
end

delays = input('Inhibitory Input Delay (ms):');
if isempty(delays)
    delays = 0;
end

k = input('Membrane Potential: 1)-55, 2)-58, 3)-60, 4)-62, 5)-65, 6)-70, 7)-75');
if isempty(k)
    k = 3;
end

gabaAcond = input('GABAa Conductance (default 4nS):');
if isempty(gabaAcond)
    gabaAcond = 4;
end

gabaBcond = input('GABAb Conductance (default 2nS):');
if isempty(gabaAcond)
    gabaBcond = 2;
end

% periods = input('Input Period (ms):');
% if isempty(periods)
%     periods = [3,5,7.5,10,13,15,20,25,30,40,50,100,150];
% %     periods = [25,100];
% end
periods = [3,5,7.5,10,13,15,20,25,30,40,50,100,150];
delayed = delays;
% delayed = [-5, -2, -1, 0, 1, 2, 5];
% delayed = [-5,0,5];
% periods = [3,5,7.5,10,13,15,20,25,30,40,50,100,150];
% periods = [5,15,20];
% periods = [5,10,15,25,50];
% periods = 100;
% periods = [5,7.5,10,12.5,15,20,25,30,50,100];
% periods = [25,100];

%% Basic Parameter Settings

LargeIC = {[16,.5],[2,1],[2]};
SmallIC = {[2,2],[4,1],[3]};
MidIC = {[2,2],[4,1],[4]}; %Mixed or PPF

syntype = {'LargeIC','SmallIC','MidIC'};
plastype = {'_None_','_PPD_','_Mixed_','_PPF_'};

vinit = [-55,-58,-60,-62,-65,-70,-75];
BiasCur = [.187,.15,.124,.094,.044,-.067,-.22];

H{1} = LargeIC;
H{2} = SmallIC;
H{3} = MidIC;

YY = 18;
dlmwrite('gabaatau.dat',YY);

ZZ = 2.8;
dlmwrite('ampatau.dat',ZZ);

% 1 = Large, 2 = Small
% Large = 1;
% Small = 2;
% Mid = 3;

%Change between Large or Small or Mid
% synsize = Small;
% synsize = Large;

%% Synaptic Inputs!!!
numberinputs = H{synsize}{2}(1);
variance = H{synsize}{2}(2);


% INdelay = delays;
dlmwrite('numinputs.dat',numberinputs);
dlmwrite('numinputsI.dat',numINs);

numins = 2;  % How many synaptic types?
%% Major Loop
for z = 1:length(periods)
    for i = 1:numtrials
        for dels = 1:length(delayed)
        period = periods(z);
        
        % Create IC Input Waveform
        gausswid = [2.4;1.15;.7;.5;.35;.25;.18;.12;.05];
        %  gaussVS =  [.9;.8;.7;.6;.5;.4;.3;.2;.1];
        w = 1;
        gausswid1 = gausswid(w);
        % Set the number of inputs here
        numinputs = numberinputs;
        
        for o = 1:numinputs
            tempinputs = PeriodicSpikes(period);
            tempshift = variance*randn(length(tempinputs),1);
            finished = tempinputs + tempshift;
            A(o).list = finished;
            strB = num2str(period);
            filenames = strcat('dumbfile',num2str(o),'.dat');
            dlmwrite(filenames,A(o).list)
        end

        for oi = 1:numINs
            INdelay = delayed(dels);
            tempin = PeriodicSpikes(period);

            tempshiftIN = variance*randn(length(tempin),1);
            INfinished = tempin + tempshiftIN + INdelay;
%             INfinished(INfinished < 0) = [];
            I(oi).list = INfinished;

            Ifile = strcat('idumbfile',num2str(oi),'.dat');
            dlmwrite(Ifile,I(oi).list)
        end
        %% Program Initializations
        
        B = H{synsize}{1}(1);
        C = H{synsize}{1}(2);
        F = B.*C;
        D = gabaAcond;
        G = gabaBcond;
        
        E = [B;F;D;G];
        %             E = [0;0;D;G];
        
        dlmwrite('paramfile.dat',E);
        BiasAmp = BiasCur(k);
        Vint = vinit(k);
        dlmwrite('biascur.dat',BiasAmp);
        dlmwrite('vinit.dat',Vint);
        
        s = H{synsize}{3};
        
        %                 clear runtype
        headpptype = char(plastype(s));
        % Define a lot of string ids
        head0 = 'Delay_';
        head0a = num2str(INdelay);
        head1 = strB;
        head2 = 'Period_';
        headtype = char(syntype(synsize));
        head3 = '_mempot';
        headmem = num2str(vinit(k));
        head4 = 'Trial_';
        headtrial = num2str(i);
        headfin = '.dat';
        gauss1 = strcat('_',num2str(gausswid1),'_');
        
        % Create the filename to save plot to
        filesav = strcat(head0,head0a,head2,head1,headpptype,headtype,head3,headmem,head4,headtrial,headfin);
%         filesav = strcat(head2,head1,headpptype,headtype,gauss1,head3,headmem,head4,headtrial,headfin);
        fid = fopen('filesav.dat', 'wt');
        fprintf(fid, '%s', filesav);
        fclose(fid)
        
        if s == 1
            [status,result] = system('C:\nrn73\bin\nrniv.exe -nobanner SingleCompNeuron4.hoc -c quit()');
        elseif s == 2
            [status,result] = system('C:\nrn73\bin\nrniv.exe -nobanner SingleCompNeuronPPD4.hoc -c quit()');
        elseif s == 3
            [status,result] = system('C:\nrn73\bin\nrniv.exe -nobanner SingleCompNeuronMix4.hoc -c quit()');
        else
            [status,result] = system('C:\nrn73\bin\nrniv.exe -nobanner SingleCompNeuronPPF4.hoc -c quit()');
        end
        end
    end
end
synsiz = synsize;
plas = s;
mempot = k;
InMGBanalysis(periods,numtrials,synsiz,mempot,plas,delays,plastype)
MGBrMTFtMTF(periods,numtrials,synsiz,mempot,plas,delays)
% load RAYData2
% load TrialSPK2
% load VSData2
% numeranal2


