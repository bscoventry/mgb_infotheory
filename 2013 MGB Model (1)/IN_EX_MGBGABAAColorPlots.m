function IN_EX_MGBGABAAColorPlots

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create GABAa conductance vs. Timing Color Plots!!!

numtrials = 25;
period = input('Period (ms):');
numINs = 1;


delayed = [-5, -3, -2, -1, 0, 1, 2, 3, 5];
k = 3;
gabaAconds = linspace(0,20,11);
gabaBcond = 0;


%% Basic Parameter Settings
%*** Increased G Parameters ***
% LargeIC = {[16,1.25],[2,1],[2]};
% SmallIC = {[2,2.25],[4,1],[3]};
% MidIC = {[2,2.25],[4,1],[4]};

%*** Below Used for MGBEx Manuscript****
LargeIC = {[16,.5],[2,1],[2]};
SmallIC = {[2,2],[4,1],[3]};
MidIC = {[2,2],[4,1],[4]}; %PPF

syntype = {'LargeIC','SmallIC','MidIC'};
plastype = {'_None_','_PPD_','_Mixed_','_PPF_'};

vinit = [-55,-58,-60,-62,-65,-70,-75];
BiasCur = [.187,.15,.124,.094,.044,-.067,-.22];

H{1} = LargeIC;
H{2} = SmallIC;
H{3} = MidIC;

YY = 18;
dlmwrite('gabaatau.dat',YY);

ZZ = 2.8;
dlmwrite('ampatau.dat',ZZ);

% 1 = Large, 2 = Small
% Large = 1;
% Small = 2;
% Mid = 3;

%Change between Large or Small or Mid
% synsize = Small;
% synsize = Large;


%% Major Loop
% for z = 1:length(periods)
for i = 1:numtrials
    for synsize = 1:3
        numberinputs = H{synsize}{2}(1);
        variance = H{synsize}{2}(2);
        dlmwrite('numinputs.dat',numberinputs);
        dlmwrite('numinputsI.dat',numINs);
        for dels = 1:length(delayed)
            for ga = 1:length(gabaAconds)
                gausswid = [2.4;1.15;.7;.5;.35;.25;.18;.12;.05];
                %         gaussVS =  [.9;.8;.7;.6;.5;.4;.3;.2;.1];
                w = 1;
                gausswid1 = gausswid(w);
                
                % Set the number of inputs here
                numinputs = numberinputs;
                
                for o = 1:numinputs
                    tempinputs = PeriodicSpikes(period);
                    
                    tempshift = variance*randn(length(tempinputs),1);
                    finished = tempinputs + tempshift;
%                     finished(finished < 0) = 0;
                    A(o).list = finished;
                    %             str0 = 'ICTestData\';
                    strB = num2str(period);
                    
                    filenames = strcat('dumbfile',num2str(o),'.dat');
                    dlmwrite(filenames,A(o).list)
                end
                
                for oi = 1:numINs
                    INdelay = delayed(dels);
                    tempin = PeriodicSpikes(period);
                    
                    tempshiftIN = variance*randn(length(tempin),1);
                    INfinished = tempin + tempshiftIN + INdelay;
%                     INfinished(INfinished < 0) = [];
                    I(oi).list = INfinished;
                    
                    Ifile = strcat('idumbfile',num2str(oi),'.dat');
                    dlmwrite(Ifile,I(oi).list)
                end
                %% Program Initializations
                
                B = H{synsize}{1}(1);
                C = H{synsize}{1}(2);
                F = B.*C;
                gabaAcond = gabaAconds(ga);
                D = gabaAcond;
                G = gabaBcond;
                
                E = [B;F;D;G];
                
                dlmwrite('paramfile.dat',E);
                BiasAmp = BiasCur(k);
                Vint = vinit(k);
                dlmwrite('biascur.dat',BiasAmp);
                dlmwrite('vinit.dat',Vint);
                
                s = H{synsize}{3};
                
                headpptype = char(plastype(s));
                % Define a lot of string ids
                head0 = 'Delay_';
                head0a = num2str(INdelay);
                head0b = 'GABAa';
                head0c = num2str(gabaAcond);
                head1 = strB;
                head2 = '_Period_';
                headtype = char(syntype(synsize));
                %         head3 = '_mempot';
                %         headmem = num2str(vinit(k));
                head4 = 'Trial_';
                headtrial = num2str(i);
                headfin = '.dat';
                
                % Create the filename to save plot to
                filesav = strcat(head0,head0a,head0b,head0c,head2,head1,headpptype,headtype,head4,headtrial,headfin);
                fid = fopen('filesav.dat', 'wt');
                fprintf(fid, '%s', filesav);
                fclose(fid)
                
                if s == 1
                    [status,result] = system('C:\nrn71\bin\nrniv.exe -nobanner SingleCompNeuron4.hoc -c quit()');
                elseif s == 2
                    [status,result] = system('C:\nrn71\bin\nrniv.exe -nobanner SingleCompNeuronPPD4.hoc -c quit()');
                elseif s == 3
                    [status,result] = system('C:\nrn71\bin\nrniv.exe -nobanner SingleCompNeuronMix4.hoc -c quit()');
                else
                    [status,result] = system('C:\nrn71\bin\nrniv.exe -nobanner SingleCompNeuronPPF4.hoc -c quit()');
                end
            end
        end
    end
end
% end

GABAAColorPlotsRead(period)
GABAAcolorprint(period)

