function MembraneFluctAnalysis(periods,done,synsiz,mempot,plas,delo)

syntype = {'LargeIC','SmallIC','MidIC'};
vinit = [-55,-58,-60,-62,-65,-70,-75];
% j = synsiz;
k = mempot;
z = plas;
delayed = delo;

for dels = 1:length(delayed)
for i = 1:length(periods)
    INdelay = delayed(dels);
    head0 = 'Delay_';
    head0a = num2str(INdelay);

w = 1;    

period = periods(i);
headtype = char(syntype(synsiz));
strB = num2str(period);
head1 = strB;
head2 = 'Period_';
head3 = '_mempot';
headmem = num2str(vinit(k));
head4 = 'Trial_';
filesav = strcat(head0,head0a,head2,head1,headtype,head3,headmem,head4);

for g = 1:done
    fstring = strcat(filesav,num2str(g),'.dat');
    data = dlmread(fstring,'\t');
    data(data > -35) = -35;
    avgtrace(g) = mean(data);
end
newdattest = data;
memstdtrial = std(data);
newdat = avgtrace;
avgtrial = mean(avgtrace);
stdtrial = std(avgtrace);

save('MFavg2.mat','avgtrial')
save('MFstd2.mat','stdtrial')
save('Mempotentest.mat','newdat')
save('IMemTest2.mat','memstdtrial')
end
end



