TITLE Ampa synapse

COMMENT
        simple alpha-synapse that generates a single PSP   
        *********************************************
        reference:      McCormick, Wang & Huguenard (1993) 
			Cerebral Cortex 3(5), 387-398
        *********************************************
	
	Revised by CRabang (2010) 
ENDCOMMENT
					       
INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	POINT_PROCESS IC_AMPA
	RANGE e, i, g, w, tau, a1, tauF, tauR, tau1
	NONSPECIFIC_CURRENT i
}

UNITS {
	(nA) 	= (nanoamp)
	(mV)	= (millivolt)
	(nS) 	= (nanomho)
}

PARAMETER {
	tau1 = .5464	(ms)
	:tau = 6	(ms)
	tau = 1.4
	tauF = .3	(ms)
	tauR = 13.3	(ms)
	a1 = 3.65
	w= .001				: weight factor for gmaxEPSP
	e= 0.0		(mV)
	v		(mV)
	celsius		(degC)
}

ASSIGNED { 
	i 		(nA)  
	g 		(nS)
}

STATE {
	A (nS)
	B (nS)
}

UNITSOFF
INITIAL {

	A = 0
	B = 0
}


BREAKPOINT { 
	SOLVE state METHOD cnexp
	g = (B - A)
	i = 1.8462*g*(v - e)
}
DERIVATIVE state {
	A' = -A/tau1
	B' = -B/tau
}

UNITSON

NET_RECEIVE(weight (nS), t0 (ms)){
LOCAL td
INITIAL {t0 = t}
td = (t-t0)

A = A + .001 * weight 
B = B + .001 * weight 
t0 = t
}
