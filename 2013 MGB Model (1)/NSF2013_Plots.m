%--------------------------------------------------------------------------
% Purpose: Generates plots for MGB_NSF2013
%--------------------------------------------------------------------------

RL10 = load('Large10Prob/Rate2.mat');   %Load in Rate data from Large10Prob
RsL10 = load('Large10Prob/RateStd2.mat');
RseL10 = load('Large10Prob/IMemTest2.mat');
ML10 = load('Large10Prob/Mempotentest.mat');

RL20 = load('Large20Prob/Rate2.mat');   %Load in Rate data from Large10Prob
RsL20 = load('Large20Prob/RateStd2.mat');
RseL20 = load('Large20Prob/IMemTest2.mat');
ML20 = load('Large20Prob/Mempotentest.mat');

RL25 = load('Large25Prob/Rate2.mat');   %Load in Rate data from Large10Prob
RsL25 = load('Large25Prob/RateStd2.mat');
RseL25 = load('Large25Prob/IMemTest2.mat');
ML25 = load('Large25Prob/Mempotentest.mat');

RL50 = load('Large50Prob/Rate2.mat');   %Load in Rate data from Large10Prob
RsL50 = load('Large50Prob/RateStd2.mat');
RseL50 = load('Large50Prob/IMemTest2.mat');
ML50 = load('Large50Prob/Mempotentest.mat');

RL75 = load('Large75Prob/Rate2.mat');   %Load in Rate data from Large10Prob
RsL75 = load('Large75Prob/RateStd2.mat');
RseL75 = load('Large75Prob/IMemTest2.mat');
ML75 = load('Large75Prob/Mempotentest.mat');

RL100 = load('Large100Prob/Rate2.mat'); %Load in Rate data from Large10Prob
RsL100 = load('Large100Prob/RateStd2.mat');
RseL100 = load('Large100Prob/IMemTest2.mat');
ML100 = load('Large100Prob/Mempotentest.mat');

RS10 = load('Small10Prob/Rate2.mat');   % " " Small10Prob
RsS10 = load('Small10Prob/RateStd2.mat');
RseS10 = load('Small10Prob/IMemTest2.mat');
MS10 = load('Small10Prob/Mempotentest.mat');

RS20 = load('Small20Prob/Rate2.mat');   % " " Small10Prob
RsS20 = load('Small20Prob/RateStd2.mat');
RseS20 = load('Small20Prob/IMemTest2.mat');
MS20 = load('Small20Prob/Mempotentest.mat');

RS25 = load('Small25Prob/Rate2.mat');   % " " Small10Prob
RsS25 = load('Small25Prob/RateStd2.mat');
RseS25 = load('Small25Prob/IMemTest2.mat');
MS25 = load('Small25Prob/Mempotentest.mat');

RS50 = load('Small50Prob/Rate2.mat');   % " " Small10Prob
RsS50 = load('Small50Prob/RateStd2.mat');
RseS50 = load('Small50Prob/IMemTest2.mat');
MS50 = load('Small50Prob/Mempotentest.mat');

RS75 = load('Small75Prob/Rate2.mat');   % " " Small10Prob
RsS75 = load('Small75Prob/RateStd2.mat');
RseS75 = load('Small75Prob/IMemTest2.mat');
MS75 = load('Small75Prob/Mempotentest.mat');

RS100 = load('Small100Prob/Rate2.mat');   % " " Small10Prob
RsS100 = load('Small100Prob/RateStd2.mat');
RseS100 = load('Small100Prob/IMemTest2.mat');
MS100 = load('Small100Prob/Mempotentest.mat');

mempotenfieldl = [RseL10 RseL20 RseL25 RseL50 RseL75 RseL100];
mempotenl = zeros(1,6);
for ii = 1:6
    mempotenl(ii) = getfield(mempotenfieldl(ii),'memstdtrial');
end

stdfirfieldl = [RsL10 RsL20 RsL25 RsL50 RsL75 RsL100];
stdfirl = zeros(1,6);
for ii = 1:6
    stdfirl(ii) = getfield(stdfirfieldl(ii),'perstd');
end

mrfieldl = [RL10 RL20 RL25 RL50 RL75 RL100];
mrl = zeros(1,6);
for ii = 1:6
    mrl(ii) = getfield(mrfieldl(ii),'permean');
end

mpfieldl = [ML10 ML20 ML25 ML50 ML75 ML100];
mpl = zeros(1,6);
for ii = 1:6
    meanval = mean(getfield(mpfieldl(ii),'newdat'));
    mpl(ii) = meanval;
end

firprob = [10 20 25 50 75 100];


%Generate Graphs for small input
mempotenfields = [RseS10 RseS20 RseS25 RseS50 RseS75 RseS100];
mempotens = zeros(1,6);
for ii = 1:6
    mempotens(ii) = getfield(mempotenfields(ii),'memstdtrial');
end

stdfirfields = [RsS10 RsS20 RsS25 RsS50 RsS75 RsS100];
stdfirs = zeros(1,6);
for ii = 1:6
    stdfirs(ii) = getfield(stdfirfields(ii),'perstd');
end

mrfields = [RS10 RS20 RS25 RS50 RS75 RS100];
mrs = zeros(1,6);
for ii = 1:6
    mrs(ii) = getfield(mrfields(ii),'permean');
end

%mpfields
%Now determine Coefficient of Variation: CoV = standdev/mean
for jj = 1:6
    CoVl(jj) = stdfirl(jj)/mrl(jj);
    CoVs(jj) = stdfirs(jj)/mrs(jj);
end

mpfields = [MS10 MS20 MS25 MS50 MS75 MS100];
mps = zeros(1,6);
for ii = 1:6
    meanval = mean(getfield(mpfields(ii),'newdat'));
    mps(ii) = meanval;
end


%First plot: Membrane Potential vs. firing probability for Large Input
figure(1)
plot(firprob,mempotenl,firprob,mempotens)
xlabel('Firing Probability')
ylabel('Membrane Potential Standard Deviation (mV)')
legend('Large Input','Small Input')

%Second plot: STD vs. firing probability for Large Input
figure(2)
plot(firprob,stdfirl,firprob,stdfirs)
xlabel('Firing Probability')
ylabel('Firing Rate Standard Deviation (Spikes/s)')
legend('Large Input','Small Input')

%Third Plot: Mean Rate vs. firing prob for Large Input
figure(3)
plot(firprob,mrl,firprob,mrs)
xlabel('Firing Probability')
ylabel('Mean Firing Rate (Spikes/s)')
legend('Large Input','Small Input')

%Fourth Plot: Coefficient of Variance
figure(4)
plot(firprob,CoVl,firprob,CoVs)
xlabel('Firing Probability')
ylabel('Coefficient of Variation')
legend('Large Input','Small Input')

memrest = -60;
deltl = mpl - memrest;
delts = mps - memrest;
%Fifth Plot: Delta Mem_Pot and std Mem_Pot for large input
figure(5)
plot(firprob,deltl,firprob,delts)
xlabel('Firing Probability')
ylabel('Difference of Vmem and Resting Potential (mV)')
legend('Large Input','Small Input')