TITLE IC IN/EX NMDA Synapse

COMMENT
	simple alpha-synapse that generates a single PSP 
        *********************************************
        reference:      McCormick, Wang & Huguenard (1993)
                        Cerebral Cortex 3(5), 387-398
        *********************************************
	Assembled for MyFirstNEURON by Arthur Houweling
ENDCOMMENT
					       
INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	POINT_PROCESS IC_NMDA_PPD
	USEION mg READ mgo VALENCE 2
	RANGE e, g, i, w, tau, a1, tauF, tauR, tau1, presynG
	NONSPECIFIC_CURRENT i
}

UNITS {
	(nA) 	= (nanoamp)
	(mV)	= (millivolt)
	(nS) 	= (nanomho)
	(mM)    = (milli/liter)
        F	= 96480 (coul)
        R       = 8.314 (volt-coul/degC)

}

PARAMETER {
	:tau = 71
	tau = 40	(ms)
	tau1 = 13.2
	tauF = .3	(ms)
	tauR = 41	(ms)		
	a1 = .95
	:a1 = .75	
	w= .001				: weight factor for gmaxEPSP
	e= 0		(mV)
	v		(mV)
	mgo		(mM)
	celsius 	(degC)
	presynG = 1
}

ASSIGNED { 
	i (nA)  
	g (nS)
}

STATE {
	A (nS)
	B (nS)
}

UNITSOFF
INITIAL {

	A = 0
	B = 0
}



BREAKPOINT {	LOCAL k
	SOLVE state METHOD cnexp
	:k = 1.07 * exp(2*0.73*v*(.001)*F/(R*(celsius+273.16)))	 
	:g = (B - A)
	:i = 2.5*g*(1-(1/(1+k/mgo)))*(v - e)
	k = 1/(1 + 0.28*exp(-0.062*v))
	g = (B - A)
	:i = 2.5*g*(1-(1/(1+k/mgo)))*(v - e)
	i = 2.5*g*k*(v - e)
}

DERIVATIVE state {
	A' = -A/tau1
	B' = -B/tau
}

UNITSON

NET_RECEIVE(weight (nS), t0 (ms), t1 (ms)){
LOCAL td, td2
INITIAL {t0 = t t1 = t}
td = (t-t0)

A = A + .001 * weight  * (1 - presynG*(a1*exp(-td/tauR) - a1*exp(-td/tauF))) * (1 - presynG*(a1*exp(-td2/tauR) - a1*exp(-td2/tauF)))
B = B + .001 * weight  * (1 - presynG*(a1*exp(-td/tauR) - a1*exp(-td/tauF))) * (1 - presynG*(a1*exp(-td2/tauR) - a1*exp(-td2/tauF)))

t1 = t0
t0 = t
}



