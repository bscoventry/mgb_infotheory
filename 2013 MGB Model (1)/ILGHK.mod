INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	SUFFIX ilGHK
	USEION ca READ cai,cao WRITE ica
	RANGE pcabar, m_inf, tau_m, m_alpha, m_beta, shift, actshift
	GLOBAL qm, qh
}


UNITS {
	(molar) = (1/liter)
	(mV) =	(millivolt)
	(mA) =	(milliamp)
	(mM) =	(millimolar)

	FARADAY = (faraday) (coulomb)
	R = (k-mole) (joule/degC)
}

PARAMETER {
	v		(mV)
	celsius	= 36	(degC)
	pcabar = 0.000276	(cm/s)
	:pcabar	=.2e-3	(cm/s)	: Maximum Permeability (to be changed - use Huguenard 92)
	: pcabar	= 0	(cm/s)
	shift	= 2 	(mV)	: corresponds to 2mM ext Ca++
	actshift = 0 	(mV)	: shift of activation curve (towards hyperpol)
	cai	= 2.4e-4 (mM)	: adjusted for eca=120 mV
	cao	= 2	(mM)
	qm	= 5		: q10's for activation and inactivation
	qh	= 3		: from Coulter et al., J Physiol 414: 587, 1989
}

STATE {
	m
}

ASSIGNED {
	ica	(mA/cm2)
	m_inf
	tau_m	(ms)
	phi_m
	m_alpha
	m_beta
}

BREAKPOINT {
	SOLVE castate METHOD euler
	ica = pcabar * m*m * ghk(v, cai, cao)
}

DERIVATIVE castate {
	evaluate_fct(v)

	m' = (m_inf - m) / tau_m
}


UNITSOFF
INITIAL {
	phi_m = qm ^ ((celsius-24)/10)
	

	evaluate_fct(v)

	m = m_inf
}

PROCEDURE evaluate_fct(v(mV)) {

	m_alpha = 1.6/(1 + exp(-0.072*(v - 5.0)))
	m_beta = 0.02*(v - 1.31)/(exp((v - 1.31)/5.36) - 1)
	m_inf = m_alpha/(m_alpha + m_beta)
	
	tau_m = 1/(m_alpha + m_beta)
}	

FUNCTION ghk(v(mV), ci(mM), co(mM)) (.001 coul/cm3) {
	LOCAL z, eci, eco
	z = (1e-3)*2*FARADAY*v/(R*(celsius+273.15))
	eco = co*efun(z)
	eci = ci*efun(-z)
	:high cao charge moves inward
	:negative potential charge moves inward
	ghk = (.001)*2*FARADAY*(eci - eco)
}

FUNCTION efun(z) {
	if (fabs(z) < 1e-4) {
		efun = 1 - z/2
	}else{
		efun = z/(exp(z) - 1)
	}
}
FUNCTION nongat(v,cai,cao) {	: non gated current
	nongat = pcabar * ghk(v, cai, cao)
}
UNITSON